For more info, see:
Troubleshooting TCP Unidirectional Data Transfer Throughput: Packet Trace Analysis Using Wireshark by Kinson Ho (VMware, Sep 2023)

https://blogs.vmware.com/performance/2023/09/troubleshooting-tcp-unidirectional-data-transfer-throughput-with-wireshark.html

https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/techpaper/performance/ts-tcp-unidir-wireshark-perf.pdf
