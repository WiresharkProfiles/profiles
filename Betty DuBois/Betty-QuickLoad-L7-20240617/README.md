# QuickLoad L7
Created by Betty DuBois of Packet Detectives. The goal is to load a large file as quickly as possible, then filter, export, and work with the new smaller file. Dissects to application layer, but most dissectors are turned off. All preferences for the dissectors still in use, are turned off.

## What did I change?

### Color rules
I standardized my color rules, and use the same set for all of my profiles.  
* Red - troubleshooting
* Butter yellow - notice but don’t freak out
* Green - good for the network team i.e. TCP Window issues
* Blue - VoIP 
* Orange - weird (HT to the Zeek weird.log)
* White backgrounds are delineated by a color for each layer of the OSI model

### Display Filter Buttons
Thanks to Filter Button Groups a/k/a FBGs, I can group my filters. Now I use the same filter set in all of my profiles.  They are grouped by OSI Model layer, Frame (metadata), and General.

### Columns
I added Delta Time Displayed (time from the end of a packet to the next one that displays through the filter), Delta Time Application (same thing, but from an Application layer request to response packet), which color rule applies,TCP initial round trip time, Delta Time TCP (same as Delta Displayed but for a single stream without having to filter), and UDP or TCP stream #. 

### Enabled Protocols
I pulled out as many as possible to make Wireshark faster.  If something isn’t dissecting (it only shows as Data in the details), flip to the Wireshark Default profile and see if it now dissects.  Add that protocol in Analyze>Enabled Protocols.

Each new version of Wireshark adds new dissector files, so I have to keep going back in and deselecting the new ones.

These protocols (plus a few more) are on in this profile.  Ethernet, Raw, STP, VLAN, VTP, GRE, CDP, IP, IP6, ICMP,ICMP6, DHCP, RIP, EIGRP, OSPF, VXLAN, UDP, TCP, DNS, TLS, HTTP1|2|3, SMB, SMB2, DECRPC, RPC, RTP, all the VoIPs, and NFS.

### Suggestions
If you have suggestions for this, or any other of my profiles, please let me know - profiles@bettydubois.com.

If you have a profile you would like to share, let me know that too. I'd like to add it to https://gitlab.com/WiresharkProfiles/profiles and credit you as the contributor.

### How to Add to Wireshark
In Wireshark, go to “Edit>Configuration Profiles>Import>from zip file” and browse to the downloaded zip file.

### Disclaimer
Download and use at your own risk, I make no warranties or guaranties.