# Betty-WiFi

Created by Betty DuBois of Packet Detectives.

Modified version of the Wireshark Default profile. Different colors, additional columns and filters. I build all of my other profiles from this one. 

## What did I change?

### Color rules
I standardized my color rules, and use the same set for all of my profiles.  
* Red - troubleshooting
* Butter yellow - notice but don’t freak out
* Green - good for the network team i.e. TCP Window issues
* Blue - VoIP or 802.11
* Orange - weird (HT to the Zeek weird.log)
* White backgrounds are delineated by a color for each layer of the OSI model

### Display Filter Buttons
Thanks to Filter Button Groups a/k/a FBGs, I can group my filters. Now I use the same filter set in all of my profiles.  They are grouped by OSI Model layer, Frame (metadata), and General.

### Columns

* Delta Time Displayed (time from the end of a packet to the next one that displays through the filter)
* 802.11 type and subtype
* Color Rule Name
* RSSI
* SNR - Signal to Noise Ratio
* Channel
* SSID
* There are also a few hidden columns that are helpful ws

### Enabled Protocols
I left all protocols enabled. You can always toggle off what you don’t need to make Wireshark run faster.

### Suggestions
If you have suggestions for this, or any other of my profiles, please let me know - profiles@bettydubois.com.

If you have a profile you would like to share, let me know that too. I'd like to add it to https://gitlab.com/WiresharkProfiles/profiles and credit you as the contributor.

### How to Add to Wireshark
In Wireshark, go to “Edit>Configuration Profiles>Import>from zip file” and browse to the downloaded zip file.

### Disclaimer
Download and use at your own risk, I make no warranties or guaranties.